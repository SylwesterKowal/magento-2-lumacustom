<?php

namespace Kowal\Lumacustom\Observer;

use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\UrlInterface;

class CustomCatalogMenuObserver implements ObserverInterface
{

    /**
     * @var nodeFactory
     */
    private $nodeFactory;


    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    public function __construct(
        NodeFactory $nodeFactory,
        UrlInterface $urlBuilder
    ) {
        $this->nodeFactory = $nodeFactory;
        $this->urlBuilder = $urlBuilder;
    }

    public function execute(Observer $observer)
    {
//        if (!$this->configProvider->isAddToMainMenu() || !$this->configProvider->isEnabled()) {
//            return;
//        }

        /** @var Node $menu */
        $menu = $observer->getMenu();
        $url = $this->urlBuilder->getUrl('contact');

        $node = $this->nodeFactory->create(
            [
                'data' => [
                    'name'   => __("Contact"),
                    'id'     => 'contact-category-link',
                    'url'    => $url
                ],
                'idField' => 'id',
                'tree' => $menu->getTree(),
                'parent' => $menu
            ]
        );
        $menu->addChild($node);
    }
}
