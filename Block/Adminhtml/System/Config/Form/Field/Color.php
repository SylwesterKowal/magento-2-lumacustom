<?php

namespace Kowal\Lumacustom\Block\Adminhtml\System\Config\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Color
 * @package Kowal\Base\Block\Adminhtml\System\Config\Form\Field
 */
class Color extends Field
{
	/**
	 * @param AbstractElement $element
	 * @return string
	 */
	protected function _getElementHtml(AbstractElement $element)
	{
		$html = $element->getElementHtml();
		$html .= '<script type="text/javascript">var picker = document.getElementById("'. $element->getHtmlId() .'"); picker.className = picker.className + " jscolor {required:false,hash:true}"; </script>';
		return $html;
	}
}
