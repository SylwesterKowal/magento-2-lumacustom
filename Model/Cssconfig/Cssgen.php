<?php


namespace Kowal\Lumacustom\Model\Cssconfig;

use Exception;
use Magento\Directory\Helper\Data;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Design\Theme\ThemeProviderInterface;
use Magento\Framework\View\Design\ThemeInterface;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Theme\Model\Theme;

//use Kowal\Lumacustom\Helper\Cssconfig;

class Cssgen
{
    protected $_storeManager;
    protected $_coreRegistry;
    protected $_layoutManager;
    protected $_messageManger;
    protected $_fileSystem;
    protected $_themeModel;
    protected $_scopeConfig;
//    protected $_cssConfig;
    protected $_themeProvider;

    public function __construct(
        StoreManagerInterface $storeManager,
        Registry $coreRegistry,
        LayoutInterface $layoutManager,
        ManagerInterface $messageManager,
        Filesystem $filesystem,
        Theme $themeModel,
        ScopeConfigInterface $scopeConfig,
//        Cssconfig $cssConfig,
        DirectoryList $directoryList,
        ThemeProviderInterface $themeProvider
    )
    {
        $this->_storeManager = $storeManager;
        $this->_coreRegistry = $coreRegistry;
        $this->_layoutManager = $layoutManager;
        $this->_messageManager = $messageManager;
        $this->_fileSystem = $filesystem;
        $this->_themeModel = $themeModel;
        $this->_scopeConfig = $scopeConfig;
//        $this->_cssConfig = $cssConfig;
        $this->_directoryList = $directoryList;
        $this->_themeProvider = $themeProvider;
    }

    public function generateCss($websiteId, $storeId)
    {
        $websites = $this->_storeManager->getWebsites();
        foreach ($websites as $website) {
            $this->generateWebsiteCss($website);
        }

//        if (!$websiteId && !$storeId) {
//            $websites = $this->_storeManager->getWebsites();
//            foreach ($websites as $website) {
//                $this->generateWebsiteCss($website);
//            }
//        } else {
//            if ($storeId) {
//                $this->generateStoreCss($storeId);
//            } else {
//                $this->generateWebsiteCss($websiteId);
//            }
//        }
    }

    protected function generateWebsiteCss($website)
    {
        if (is_object($website)) {
            foreach ($website->getStoreIds() as $storeId) {
                $this->generateStoreCss($storeId);
            }
        }
    }

    protected function generateStoreCss($storeId)
    {
        $store = $this->_storeManager->getStore($storeId);
        if (!$store->isActive())
            return;
        $cssBlockHtml = $this->_layoutManager->createBlock('Kowal\Lumacustom\Block\Template')->setStoreID($storeId)->setTemplate("Kowal_Lumacustom::lumacustom.phtml")->toHtml();
        $this->_coreRegistry->register('ct_store', $store);

        try {

            if (empty($cssBlockHtml)) {
                throw new Exception(__("Template file is empty or doesn't exist."));
            }

            $dir = $this->_fileSystem->getDirectoryWrite('app');

            $fileName = $this->getThemePath($storeId); // nowa wersja do less

            $dir->writeFile($fileName, $cssBlockHtml);
            $this->_messageManager->addSuccess(__('The %1 file updated successfully.', $dir->getAbsolutePath($fileName)));

        } catch (Exception $e) {
            $this->_messageManager->addError(__('Failed generating CSS file'));
        }
        $this->_coreRegistry->unregister('ct_store');
    }

    protected function getThemePath($storeId)
    {
        $theme = $this->getTheme($storeId);
        $app = $this->_directoryList->getPath('app');
        $dir = '/design/frontend/';

        return $dir . $theme . '/web/css/source/_extend.less';
    }


    public function getTheme($storeId)
    {
        $themeId = $this->_scopeConfig->getValue(
            DesignInterface::XML_PATH_THEME_ID,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        /** @var $theme ThemeInterface */
        $theme = $this->_themeProvider->getThemeById($themeId);

        return $theme->getThemePath();
    }
}
