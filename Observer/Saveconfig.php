<?php


namespace Kowal\Lumacustom\Observer;

use Kowal\Lumacustom\Model\Cssconfig\Cssgen;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;

;

class Saveconfig implements ObserverInterface
{
    
    protected $_messageManager;
    protected $_cssGenerator;

    public function __construct(
        Cssgen $cssGenerator,
        ManagerInterface $messageManager
    )
    {
        $this->_cssGenerator = $cssGenerator;
        $this->_messageManager = $messageManager;
    }

    public function execute(Observer $observer)
    {
        $this->_cssGenerator->generateCss($observer->getData('website'), $observer->getData('store'));
    }
}