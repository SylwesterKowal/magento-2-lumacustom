<?php

namespace Kowal\Lumacustom\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{

    protected $_storeManager;
    protected $_scopeConfig;
    protected $_coreRegistry;
    protected $storeID;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Registry $coreRegistry,
        array $data = []

    )
    {
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_storeManager = $storeManager;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }


    public function setStoreID($storeId)
    {
        $this->storeID = $storeId;
    }

    public function getConfig($config_path, $storeId = null)
    {
        $storeId = (is_null($storeId)) ? $this->storeID : $storeId;

        $store = $this->_storeManager->getStore($storeId);
        if ($this->_coreRegistry->registry('ct_store')) {
            $store = $this->_coreRegistry->registry('ct_store');
        }
        return $this->_scopeConfig->getValue(
            $config_path,
            ScopeInterface::SCOPE_STORE,
            $store
        );

    }

    public function getStoreConfig($config_path, $storeId = NULL)
    {
        if ($storeId != NULL) {
            return $this->_scopeConfig->getValue(
                $config_path,
                ScopeInterface::SCOPE_STORE,
                $storeId
            );
        }
        return $this->_scopeConfig->getValue(
            $config_path,
            ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()->getId()
        );
    }

    public function getGoogleFonts()
    {

        $fonts = [];
        if ($this->getStoreConfig('lumacustom/font_custom/font_family') == 'google') {
            $body_font = $this->getStoreConfig('lumacustom/font_custom/google_font_family');
            $fonts[] = $body_font;
        }

        if ($this->getStoreConfig('lumacustom/font_custom/button_font_family') == 'google') {
            $button_font = $this->getStoreConfig('lumacustom/font_custom/button_google_font_family');
            $fonts[] = $button_font;
        }

        if ($this->getStoreConfig('lumacustom/font_custom/menu_font_family') == 'google') {
            $menu_font = $this->getStoreConfig('lumacustom/font_custom/menu_google_font_family');
            $fonts[] = $menu_font;
        }

        if ($this->getStoreConfig('lumacustom/font_custom/page_title_font_family') == 'google') {
            $page_title_font = $this->getStoreConfig('lumacustom/font_custom/page_title_google_font_family');
            $fonts[] = $page_title_font;
        }

        if ($this->getStoreConfig('lumacustom/font_custom/block_title_font_family') == 'google') {
            $block_title_font = $this->getStoreConfig('lumacustom/font_custom/block_title_google_font_family');
            $fonts[] = $block_title_font;
        }

        if ($this->getStoreConfig('lumacustom/font_custom/price_font_family') == 'google') {
            $price_font = $this->getStoreConfig('lumacustom/font_custom/price_google_font_family');
            $fonts[] = $price_font;
        }

        if ($this->getStoreConfig('lumacustom/font_custom/product_name_font_family') == 'google') {
            $product_name_font = $this->getStoreConfig('lumacustom/font_custom/product_name_google_font_family');
            $fonts[] = $product_name_font;
        }

        $fonts = array_filter($fonts);

        $links = '';

        foreach ($fonts as $_font) {
            $links .= '<link href="//fonts.googleapis.com/css?family=' . $_font . ':400,300,300italic,400italic,700,700italic,900,900italic" rel="stylesheet" type="text/css"/>';
        }

        return $links;
    }
}
