<?php


namespace Kowal\Lumacustom\Block;

use Magento\Store\Model\ScopeInterface;

class Template extends \Magento\Framework\View\Element\Template
{
    public function getConfig($config_path, $storeCode = null)
    {
        return $this->_scopeConfig->getValue(
            $config_path,
            ScopeInterface::SCOPE_STORE,
            $storeCode
        );
    }
}
