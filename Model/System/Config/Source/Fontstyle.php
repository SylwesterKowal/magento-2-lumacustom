<?php
/**************************************
 * Date: 16-9-21
 * Project: Kowal Magento2 Theme Project
 * WebSite: http://www.cattheme.com
 * Email: gizmocn@gmail.com
 *************************************/

namespace Kowal\Lumacustom\Model\System\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Fontstyle implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => '', 'label' => __('--- Select ---')],
            ['value' => 'normal', 'label' => __('Normal')],
            ['value' => 'italic', 'label' => __('Italic')],
            ['value' => 'oblique', 'label' => __('Oblique')],
            ['value' => 'inherit', 'label' => __('Inherit')]
        ];
    }
}