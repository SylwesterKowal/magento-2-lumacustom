<?php


namespace Kowal\Lumacustom\Model\System\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Layout implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'full_width_layout', 'label' => __('Full Width')],
            ['value' => 'boxed_layout1', 'label' => __('Boxed Layout 1')],
            ['value' => 'boxed_layout2', 'label' => __('Boxed Layout 2')]
        ];
    }
}
