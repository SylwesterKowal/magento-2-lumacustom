<?php

namespace Kowal\Lumacustom\Block\Html;

use Kowal\Lumacustom\Helper\Data;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Head extends Template
{
    public function __construct(
        Context $context,
        Data $ctHelper,
        array $data = [])
    {
        parent::__construct($context, $data);
        $_pageConfig = $context->getPageConfig();

        $layout = $ctHelper->getStoreConfig('lumacustom/general/site_layout');
        $_pageConfig->addBodyClass($layout);
    }
}